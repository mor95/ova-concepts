/**
 * /**
 * @file Archivo que contiene el módulo ova-concepts
 * @namespace index
 * @module ova-concepts
 * @author Christian Arias <cristian1995amr@gmail.com>
 * @requires module:jquery
 * @requires module:lodash
 */

const $ = require('jquery');
const _ = require('lodash');
const swal = require('sweetalert2');
const stageResize = require('stage-resize');
const API = require('ova-api')
const checkTypes = require('check-types')

/**
 * module.exports Contiene la clase.
 * @class module.exports
 */
module.exports = {
    currentProps: {
        amount: 0,
        enabled: true,
        concepts: {}
    },
    APIInstance: new API(),
    init: function(){
        
    },
    renderConceptsModal: function(){
        const template = require('templates/conceptsModal.hbs');
        var $question = $(template())
                .appendTo(stageResize.currentProps.$stage)
                .find('.close-button').on('click', function(){
                    $question.hide();
                })
                .end();
        
        return $question;

    },
    renderConceptsCount: function(args){
        if(!args.$target){
            throw new Error("A target is expected.");
        }
        if(!args.$target instanceof jQuery){
            throw new Error("Invalid target. A jQuery element was expected.");
        }
        if(!args.$target.length){
            throw new Error("jQuery target not available in DOM.");
        }

        const template = require('templates/conceptsCount.hbs');
        return $(template({
                  count: 0  
                }))
                .appendTo(args.$target);
    },
    refreshConceptsModal: function(){
        var $conceptsModal = module.exports.currentProps.$conceptsModal;
        $conceptsModal.find('.concept').remove();
          var template = require('templates/newConceptsModalConcept.hbs');
        _.forEach(module.exports.currentProps.concepts, function(v, k){
            $(template({
                conceptName: k,
                conceptDescription: v
            }))
            .appendTo($conceptsModal.find('.concepts'));
        });
    },
    initializeConcepts: function(args){
        
        module.exports.currentProps.$conceptsModal = module.exports.renderConceptsModal();
        module.exports.currentProps.$conceptsCount = module.exports.renderConceptsCount(args)
        .on('click', function(){
            // module.exports.showEarnedConceptsModal();
        });

        //TODO: Handlebars
        // module.exports.currentProps.$conceptsModal = module.exports.: function(args){
        
    },   
    showEarnedConceptsModal: function(){
        var $el = module.exports.currentProps.$conceptsCount;
        var $siblings = $el.siblings();
        $siblings.removeClass('animated');
        setTimeout(function(){
            $siblings.addClass('animated');
        }, 100);
        module.exports.currentProps.$conceptsModal.show();
    },
    updateConcepts: function(args){
        checkTypes.assert.object(args);
        checkTypes.assert.object(args.concepts);
        checkTypes.assert.string(args.action);
        checkTypes.assert.string(args.courseShortName);
        checkTypes.assert.integer(args.courseUnit);

        if(args.action === 'insert'){
            return this.APIInstance.makeRequest({
                component: 'concepts',
                method: 'addLOConcepts',
                arguments: {
                    shortName: args.courseShortName,
                    unit: args.courseUnit,
                    concepts: _.keys(args.concepts)
                }
            })
            .then(function(response){
                if(response !== true){
                    swal({
                        type: 'error',
                        title: 'Error en la respuesta del servidor.',
                        text: 'Ocurrió un error al agregar los conceptos'
                    })
                    console.error(response);
                    throw new Error("Expected a true response.");
                }
                
                else{
                    _.forEach(args.concepts, function(v, k){
                        module.exports.currentProps.concepts[k] = v;
                    });
                    module.exports.refreshConcepts();
                }
            });
        }
        else{
            if(args.action === 'delete'){
                return this.APIInstance.makeRequest({
                    component: 'concepts',
                    method: 'deleteLOConcepts',
                    arguments: {
                        shortName: args.courseShortName,
                        unit: args.courseUnit,
                        concepts: _.keys(args.concepts)
                    }
                })
                .then(function(response){
                    if(response !== true){
                        swal({
                            type: 'error',
                            title: 'Error en la respuesta del servidor.',
                            text: 'Ocurrió un error al eliminar los conceptos asociados a este usuario'
                        })
                        console.error(response);
                        throw new Error("Expected a true response.");
                    }
                    
                    else{
                        _.forEach(args.concepts, function(v, k){
                            delete module.exports.currentProps.concepts[k];
                        });
                        module.exports.refreshConcepts();
                    }
                });
            }
        }
    },
    refreshConcepts: function(){
        var count = _.size(module.exports.currentProps.concepts);
        var $element = module.exports.currentProps.$conceptsCount;
        $element.html(count);
        var $siblings = $element.siblings();
        $siblings.removeClass('animated');
        setTimeout(function(){
            $siblings.addClass('animated glow');
        }, 100);
        setTimeout(function(){
            $siblings.removeClass('glow');
        }, 1000);
        module.exports.refreshConceptsModal();
    },
    showNewConceptsModal: function(args){
        if(!args.title){
            throw new Error("A title is expected.");
        }
        if(typeof args.title !== 'string'){
            throw new Error("Wrong type of title.");
        }
        if(!args.concepts){
            throw new Error("A concept was expected.");
        }
        if(!_.isObject(args.concepts)){
            throw new Error("Invalid concepts");
        }
   
        const modalContent = require('templates/newConceptsModalContent.hbs');
        const modalConcept = require('templates/newConceptsModalConcept.hbs');
        const $modalContent = $(modalContent(args));
        const $target = $modalContent.find('.concepts');
        _.forEach(args.concepts, function(v, k){
            $(modalConcept({
                conceptName: k,
                conceptDescription: v
            }))
            .appendTo($target);
        });
        
        return swal({
            target: stageResize.currentProps.$stage.get(0),
            html: $modalContent.html(),
            customClass: 'ova-themed concepts',
            showConfirmButton: false,
            showCloseButton: true
        });
    }
};